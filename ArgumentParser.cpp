#include <boost/program_options.hpp>
#include <iostream>
#include "ArgumentParser.h"


std::map<std::string, int> ArgumentParser::parseArguments(int argc, char **argv) {

    try {
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
                ("help,h", "Print help messages")
                ("consumers,c", po::value<int>(), "Number of consumers threads")
                ("producers,p", po::value<int>(), "Number of producers threads")
                ("buffer,b", po::value<int>(), "Buffer size");

        po::variables_map vm;
        try {
            po::store(po::parse_command_line(argc, argv, desc), vm);

            if (vm.count("help")) {
                std::cout << "Producer consume problem visualization app" << std::endl << std::endl
                          << desc << std::endl;
                exit(SUCCESS);
            }

            if (vm.count("consumers")) {
                numberOfConsumers = vm["consumers"].as<int>();


                if (numberOfConsumers <= 0) {
                    std::cout << "ERROR: " << "Number of customers must be greater than 0" << std::endl;
                    exit(ERROR_IN_COMMAND_LINE);
                }
            }

            if (vm.count("producers")) {
                numberOfProducers = vm["producers"].as<int>();

                if (numberOfProducers <= 0) {
                    std::cout << "ERROR: " << "Number of producers must be greater than 0" << std::endl;
                    exit(ERROR_IN_COMMAND_LINE);
                }
            }

            if (vm.count("buffer")) {
                bufferSize = vm["buffer"].as<int>();

                if (bufferSize <= 0) {
                    std::cout << "ERROR: " << "Buffer size must be greater than 0" << std::endl;
                    exit(ERROR_IN_COMMAND_LINE);
                }
            }

            po::notify(vm);
        }
        catch (po::error &e) {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            exit(ERROR_IN_COMMAND_LINE);
        }

        std::map<std::string, int> arguments = {
                {"consumers", numberOfConsumers},
                {"producers", numberOfProducers},
                {"buffer", bufferSize}
        };

        return arguments;
    }
    catch (std::exception &e) {
        std::cerr << "Unhandled Exception reached the top of main: "
                  << e.what() << ", application will now exit" << std::endl;
        exit(ERROR_UNHANDLED_EXCEPTION);

    }
}
