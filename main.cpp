#include <queue>
#include <thread>
#include <sstream>
#include <ncurses.h>
#include <random>
#include <boost/program_options.hpp>
#include "Semaphore.h"
#include "NickGenerator.h"
#include "ArgumentParser.h"

void produce(int producerId, WINDOW *, WINDOW *, int, std::queue<std::string> &, bool &, bool &, std::shared_ptr<Semaphore>);

void consume(int consumerId, WINDOW *, WINDOW *, int, std::queue<std::string> &, bool &, bool &, std::shared_ptr<Semaphore>);

WINDOW *createNewWin(int height, int width, int startY, int startX);


std::mutex queueMutex;
std::mutex windowMutex;
Semaphore full(0);


int main(int argc, char **argv) {

    ArgumentParser parser;
    auto arguments = parser.parseArguments(argc, argv);

    int numberOfConsumers = arguments.at("consumers") != 0 ? arguments.at("consumers") : 24;
    int numberOfProducers = arguments.at("producers") != 0 ? arguments.at("producers") : 26;
    int bufferSize = arguments.at("buffer") != 0 ? arguments.at("buffer") : 20;

    auto empty = std::make_shared<Semaphore>(bufferSize);
    std::queue<std::string> queue;

    bool paused = false;
    bool end = false;

    initscr();
    noecho();
    cbreak();
    timeout(500);

    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_YELLOW, COLOR_BLACK);

    refresh();

    WINDOW *state = createNewWin(std::max(numberOfConsumers, numberOfProducers) + 5, 40, 0, 0);
    WINDOW *animation = createNewWin(state->_maxy + 1,
                                     std::max(std::max(numberOfConsumers, numberOfProducers), bufferSize) + 4, 0,
                                     state->_maxx + 2);

    wattron(state, COLOR_PAIR(1) | A_BOLD);
    mvwprintw(state, 1, 1, "Producers");
    wattron(state, COLOR_PAIR(2));
    mvwprintw(state, 1, 20, "Consumers");
    wattroff(state, A_BOLD | COLOR_PAIR(2));


    std::thread consumers[numberOfConsumers];
    std::thread producers[numberOfProducers];


    for (auto i = 0; i < numberOfConsumers; i++)
        consumers[i] = std::thread(consume, i, state, animation, bufferSize, std::ref(queue), std::ref(paused),
                                   std::ref(end), std::ref(empty));

    for (auto i = 0; i < numberOfProducers; i++)
        producers[i] = std::thread(produce, i, state, animation, bufferSize, std::ref(queue), std::ref(paused),
                                   std::ref(end), std::ref(empty));

    char inputChar = 0;
    while (inputChar != 'q') {
        inputChar = (char) getch();

        if (inputChar == 'p')
            paused = true;
        else if (inputChar == 'r')
            paused = false;
    }
    end = true;
    nocbreak();

    for (auto i = 0; i < numberOfConsumers; i++)
        consumers[i].join();
    for (auto i = 0; i < numberOfProducers; i++)
        producers[i].join();

    delwin(state);
    delwin(animation);
    endwin();

    return 0;
}


WINDOW *createNewWin(int height, int width, int startY, int startX) {
    WINDOW *localWin;

    localWin = newwin(height, width, startY, startX);
    box(localWin, 0, 0);
    wrefresh(localWin);

    return localWin;
}


inline void printShopState(WINDOW *state, WINDOW *animation, int bufferSize, std::queue<std::string> &queue) {
    wattron(state, A_BOLD);
    mvwprintw(state, state->_maxy - 1, 1, "Items in shop %d%10s", queue.size(), " ");
    wattroff(state, A_BOLD);
    wrefresh(state);

    for (auto i = 0; i < bufferSize; i++) {

        if (i < queue.size())
            wattron(animation, A_BOLD | COLOR_PAIR(3));
        mvwaddch(animation, animation->_maxy / 2 + 1, (animation->_maxx / 2) + i - (bufferSize / 2), '#');
        wattroff(animation, A_BOLD | COLOR_PAIR(3));
    }
}


void produce(int producerId, WINDOW *state, WINDOW *animation, int bufferSize, std::queue<std::string> &queue,
             bool &paused, bool &end, std::shared_ptr<Semaphore> empty) {

    std::string item;
    NickGenerator generator;
    std::mt19937_64 eng{std::random_device{}()};
    std::uniform_int_distribution<> dist{400, 800};

    while (!end) {
        while (paused)
            std::this_thread::sleep_for(std::chrono::milliseconds(100));

        item = generator.generateNick();

        windowMutex.lock();

        mvwprintw(state, producerId + 2, 1, "%2d %s", producerId, item.c_str());

        wattron(animation, A_BOLD | COLOR_PAIR(1));
        mvwaddch(animation, 1, producerId + 2, ' ');
        mvwaddch(animation, animation->_maxy / 2, producerId + 2, 'O');
        wattroff(animation, A_BOLD | COLOR_PAIR(1));
        wrefresh(animation);

        windowMutex.unlock();

        std::this_thread::sleep_for(std::chrono::milliseconds{dist(eng)});

        empty->wait();

        queueMutex.lock();
        queue.push(item);
        queueMutex.unlock();

        full.notify();

        windowMutex.lock();

        mvwprintw(state, producerId + 2, 1, "%2d WAITING%8s", producerId, "");
        printShopState(state, animation, bufferSize, queue);

        wattron(animation, A_BOLD | COLOR_PAIR(1));
        mvwaddch(animation, 1, producerId + 2, 'O');
        mvwaddch(animation, animation->_maxy / 2, producerId + 2, ' ');
        wattroff(animation, A_BOLD | COLOR_PAIR(1));
        wrefresh(animation);

        windowMutex.unlock();

        std::this_thread::sleep_for(std::chrono::milliseconds{dist(eng)});
    }
}


void consume(int consumerId, WINDOW *state, WINDOW *animation, int bufferSize, std::queue<std::string> &queue,
             bool &paused, bool &end, std::shared_ptr<Semaphore> empty) {

    std::string item;
    std::mt19937_64 eng{std::random_device{}()};
    std::uniform_int_distribution<> dist{400, 800};

    while (!end) {
        while (paused)
            std::this_thread::sleep_for(std::chrono::milliseconds(100));

        windowMutex.lock();
        mvwprintw(state, consumerId + 2, 20, "%2d WAITING%6s", consumerId, "");

        wattron(animation, A_BOLD | COLOR_PAIR(2));
        mvwaddch(animation, animation->_maxy / 2 + 2, consumerId + 2, ' ');
        mvwaddch(animation, animation->_maxy - 1, consumerId + 2, 'O');
        wattroff(animation, A_BOLD | COLOR_PAIR(2));
        wrefresh(animation);

        windowMutex.unlock();

        std::this_thread::sleep_for(std::chrono::milliseconds{dist(eng)});

        full.wait();

        queueMutex.lock();
        item = queue.front();
        queue.pop();
        queueMutex.unlock();

        empty->notify();

        windowMutex.lock();

        mvwprintw(state, consumerId + 2, 20, "%2d %s", consumerId, item.c_str());
        printShopState(state, animation, bufferSize, queue);

        wattron(animation, A_BOLD | COLOR_PAIR(2));
        mvwaddch(animation, animation->_maxy / 2 + 2, consumerId + 2, 'O');
        mvwaddch(animation, animation->_maxy - 1, consumerId + 2, ' ');
        wattroff(animation, A_BOLD | COLOR_PAIR(2));
        wrefresh(animation);

        windowMutex.unlock();

        std::this_thread::sleep_for(std::chrono::milliseconds{dist(eng)});
    }
}
