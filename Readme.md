#### About program
Program visualizes producer consumer problem using ncurses library.

#### Required libraries
 - boost
 - ncurses

#### Make
In order to compile program use:
```sh
$ make
```
Clean project:
```sh
$ make clean
```

#### Program parameters
```sh
-h [ --help ]
-c [ --consumers ] arg
-p [ --producers ] arg
-b [ --buffer ] arg
```

#### Running program options
User is able to control program with keys:

`p` - pause

`r` - stop pause

`q` - quit program
 



