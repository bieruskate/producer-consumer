output: main.o arg-parser.o
	g++ -std=c++11 main.o arg-parser.o -o prod-con -lcurses -lboost_program_options -lpthread

main.o:	main.cpp
	g++ -c -std=c++11 main.cpp

arg-parser.o:   ArgumentParser.cpp
	g++ -c ArgumentParser.cpp -o arg-parser.o

clean:
	rm *.o prod-con
