#pragma once

namespace {
    const size_t ERROR_IN_COMMAND_LINE = 1;
    const size_t SUCCESS = 0;
    const size_t ERROR_UNHANDLED_EXCEPTION = 2;
}

class ArgumentParser {

public:
    std::map<std::string, int> parseArguments(int argc, char **argv);

private:
    int numberOfConsumers = 0;
    int numberOfProducers = 0;
    int bufferSize = 0;

};

