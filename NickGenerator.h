#include <iostream>
#include <sstream>
#include <stdlib.h>

class NickGenerator{

public:
    std::string generateNick() {
        std::stringstream nick;
        nick << namePrefix[(rand() % 7)] << nameStems[(rand() % 20)] << nameSuffix[(rand() % 16)];
        return nick.str();
    }

private:
    std::string namePrefix[7] = {"", "bel", "nar", "xan", "bell", "natr", "ev"};
    std::string nameSuffix[16] = {
            "", "us", "ix", "ox", "ith",
            "ath", "um", "ator", "or", "axia",
            "imus", "ais", "itur", "orex", "o",
            "y"
    };
    std::string nameStems[20] = {
            "adur", "aes", "anim", "apoll", "imac",
            "educ", "equis", "extr", "guius", "hann",
            "equi", "amora", "hum", "iace", "ille",
            "inept", "iuv", "obe", "ocul", "orbis"
    };
};